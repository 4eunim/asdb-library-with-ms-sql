(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: SQLRecipe
 * File: SQLRecipe_exit.st
 * Author: B&R
 * Created: January 04, 2011
 *******************************************************************)

PROGRAM _EXIT
	WHILE eStep <> eSTOP DO
		CASE eStep OF
		(* Finish dbConnect FUB befor close connection *)
			eCONNECT_r:
				ReceipeDBConnect();
				IF ReceipeDBConnect.status <> ERR_FUB_BUSY THEN
					eStep := eDISCONNECT;
				END_IF
		(* Finish dbExecuteSQL FUB *)
			eGET_TABLES_r,
			eCREATE_TABLE_r,
			eINSERT_DEFAULT_r,
			eGET_RECEIPE_NAMES_r,
			eLOAD_RECEIPE_r,
			eSAVE_RECEIPE_r,
			eSAVE_AS_r,
			eDELETE_RECEIPE_r,
			eRENAME_RECEIPE_r:
				ReceipeDBExecute();
				IF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
					eStep := eDISCONNECT;
				END_IF
		(* Finish dbGetAffectedRows FUB *)
			eSAVE_GET_AFFECTED_ROWS_r,
			eSAVE_AS_ROWS_AFFECTED_r,
			eDELETE_ROWS_AFFECTED_r,
			eRENAME_GET_AFFECTED_ROWS_r:
				ReceipeDBGetAffectedRows();
				IF ReceipeDBGetAffectedRows.status <> ERR_FUB_BUSY THEN
					eStep := eDISCONNECT;
				END_IF
		(* Finish dbFetchNextRow FUB *)
			eFETCH_NEXT_ROW_r,
			eFETCH_RECEIPE_NAME_r,
			eFETCH_VALUES_r:
				ReceipeDBFetchNextRow();
				IF ReceipeDBFetchNextRow.status <> ERR_FUB_BUSY THEN
					eStep := eDISCONNECT;
				END_IF
		(* Finish dbGetData FUB *)
			eREAD_TABLE_NAME_r,
			eREAD_RECEIPE_NAME_r,
			eGET_VALUES_r:
				ReceipeDBGetData();
				IF ReceipeDBGetData.status <> ERR_FUB_BUSY THEN
					eStep := eDISCONNECT;
				END_IF
		(* Finish dbGetErrorMessage FUB *)
			eGET_ERROR_r:
				ReceipeDBErrMsg();
				IF ReceipeDBErrMsg.status <> ERR_FUB_BUSY THEN
					eStep := eDISCONNECT;
				END_IF				
		(* Disconnect *)
			eDISCONNECT:
				ReceipeDBDisconnect.enable := TRUE;
				ReceipeDBDisconnect.connectionIdent := ConnectionIdent;
				eStep := eDISCONNECT_r;
		(* Run Disconnect FUB *)
			eDISCONNECT_r:
				ReceipeDBDisconnect();
				IF ReceipeDBDisconnect.status <> ERR_FUB_BUSY THEN
					eStep := eSTOP;
				END_IF
				
			eERROR:
				eStep := eDISCONNECT;
			ELSE
   				eStep := eDISCONNECT;			
		END_CASE
	END_WHILE;
END_PROGRAM