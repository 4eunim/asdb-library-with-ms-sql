
TYPE
	SQLSteps_enum : 
		(
		eSTOP, (*Stop - nothing to do*)
		eCONNECT, (*Connect to Database*)
		eCONNECT_r,
		eGET_TABLE_NUMBER, (*get number of tables*)
		eGET_TABLE_NUMBER_r,
		eFETCH_TABLE_NUMBER,
		eFETCH_TABLE_NUMBER_r,
		eREAD_TABLE_NUMBER,
		eREAD_TABLE_NUMBER_r,
		eGET_TABLES, (*SELECT Names of Tables in Database*)
		eGET_TABLES_r,
		eFETCH_NEXT_ROW, (*Read line by line*)
		eFETCH_NEXT_ROW_r,
		eREAD_TABLE_NAME, (*Get name out of line*)
		eREAD_TABLE_NAME_r,
		eCREATE_TABLE, (*CREATE TABLE with name if not exist*)
		eCREATE_TABLE_r,
		eINSERT_DEFAULT, (*INSERT default receipe*)
		eINSERT_DEFAULT_r,
		eGET_RECEIPE_NUMBER, (*get number of recipe*)
		eGET_RECEIPE_NUMBER_r,
		eFETCH_RECIPE_NUMBER,
		eFETCH_RECIPE_NUMBER_r,
		eREAD_RECEIPE_NUMBER,
		eREAD_RECEIPE_NUMBER_r,
		eGET_RECEIPE_NAMES := 30, (*SELECT Names of all receipes*)
		eGET_RECEIPE_NAMES_r,
		eFETCH_RECEIPE_NAME, (*read line by line*)
		eFETCH_RECEIPE_NAME_r,
		eREAD_RECEIPE_NAME, (*get name of receipe*)
		eREAD_RECEIPE_NAME_r,
		eWAIT_FOR_COMMAND := 50, (*wait for command of user*)
		eLOAD_RECEIPE, (*SELECT a specific receipe*)
		eLOAD_RECEIPE_r,
		eFETCH_VALUES, (*prepare for read receipe values*)
		eFETCH_VALUES_r,
		eGET_VALUES, (*read each value of receipe*)
		eGET_VALUES_r,
		eSAVE_RECEIPE, (*UPDATE changed values into receipe*)
		eSAVE_RECEIPE_r,
		eSAVE_GET_AFFECTED_ROWS, (*test if saved (if one row is modified)*)
		eSAVE_GET_AFFECTED_ROWS_r,
		eSAVE_AS, (*INSERT changed values into new receipe*)
		eSAVE_AS_r,
		eSAVE_AS_ROWS_AFFECTED, (*test if saved (if one row is modified)*)
		eSAVE_AS_ROWS_AFFECTED_r,
		eDELETE_RECEIPE, (*DELETE receipe*)
		eDELETE_RECEIPE_r,
		eDELETE_ROWS_AFFECTED, (*test if deleted (if one row is modified)*)
		eDELETE_ROWS_AFFECTED_r,
		eRENAME_RECEIPE, (*UPDATE name of receipe*)
		eRENAME_RECEIPE_r,
		eRENAME_GET_AFFECTED_ROWS, (*test if renamed (if one row is modified)*)
		eRENAME_GET_AFFECTED_ROWS_r,
		eDISCONNECT := 150, (*disconnect from server*)
		eDISCONNECT_r,
		eGET_ERROR := 200, (*receive error of server*)
		eGET_ERROR_r,
		eERROR, (*error step*)
		eDUMMY (*dummy step - for development*)
		);
END_TYPE

(**)

TYPE
	sqlmain_par_recipe_typ : 	STRUCT 
		price : REAL;
		setTemp : REAL;
		milk : REAL;
		sugar : REAL;
		coffee : REAL;
		water : REAL;
	END_STRUCT;
	sqlmain_par_typ : 	STRUCT 
		recipe : sqlmain_par_recipe_typ;
		sActReceipe : STRING[LEANGTH_RECIPE_NAME];
		sNewReceipe : STRING[LEANGTH_RECIPE_NAME];
		sReceipeNames : ARRAY[0..9]OF STRING[LEANGTH_RECIPE_NAME];
		uStatus : UINT; (*0 = OK; 1 = FAIL SQL QUERY; 2 = Unexpected Error*)
		sHost : STRING[40] := '192.168.20.119:1433';
		sDataBase : STRING[40] := 'coffeemachine';
		sTable : STRING[40] := 'recipe';
		sUsername : STRING[40] := 'bur';
		sPassword : STRING[40] := 'buruser';
	END_STRUCT;
	sqlmain_status_typ : 	STRUCT 
		DisplayStep : SQLSteps_enum; (*only display current step*)
		ConnectionIdent : UDINT;
		RecipeCounter : UDINT; (*number of Recipe, list*)
		TableCounter : UDINT; (*number of table*)
		statusID : UINT;
		dbError : INT; (*0 = OK; 1 = FAIL SQL QUERY; 2 = Unexpected Error*)
		sErrorString : STRING[100];
	END_STRUCT;
	sqlmain_cmd_typ : 	STRUCT 
		Connect : BOOL;
		DisConnect : BOOL;
		LoadRecipe : BOOL;
		SaveRecipe : BOOL;
		SaveAsRecipe : BOOL;
		DeleteRecipe : BOOL;
		RenameRecipe : BOOL;
		Acknowledge : BOOL;
	END_STRUCT;
	sqlmain_typ : 	STRUCT 
		cmd : sqlmain_cmd_typ;
		par : sqlmain_par_typ;
		status : sqlmain_status_typ;
	END_STRUCT;
END_TYPE
