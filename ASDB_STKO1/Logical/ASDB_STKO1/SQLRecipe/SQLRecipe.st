(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: SQLRecipe
 * File: SQLRecipe.st
 * Author: B&R
 * Created: December 27, 2010
 * Updated: 2022/Apr/19
 ********************************************************************
 * Implementation of program SQLRecipe
 * Updated on 2022/Apr/19
 ********************************************************************)

PROGRAM _INIT

	(* copy basic setting *)
	brsstrcpy( ADR(gSQLCtrl.par.sHost), ADR(HOST) );
	brsstrcpy( ADR(gSQLCtrl.par.sDataBase), ADR(DATABASE) );
	brsstrcpy( ADR(gSQLCtrl.par.sTable), ADR(TABLE) );
	brsstrcpy( ADR(gSQLCtrl.par.sUsername), ADR(USERNAME) );
	brsstrcpy( ADR(gSQLCtrl.par.sPassword), ADR(PASSWORD) );


END_PROGRAM


PROGRAM _CYCLIC

	
	
	CASE eStep OF
		eSTOP:
			IF gSQLCtrl.cmd.Connect  THEN
				gSQLCtrl.cmd.Connect	:= FALSE;
				eStep := eCONNECT;
			END_IF;
		
		
		
		(* connect to database *)
		eCONNECT :
			brsmemset( ADR(ReceipeDBConnect), 0, SIZEOF(ReceipeDBConnect) );
			ReceipeDBConnect.enable 		:= TRUE;
			ReceipeDBConnect.databaseSystem := DB_SYSTEM_MS_SQL;
			ReceipeDBConnect.pUserName 		:= ADR(gSQLCtrl.par.sUsername);
			ReceipeDBConnect.pPassword 		:= ADR(gSQLCtrl.par.sPassword);
			ReceipeDBConnect.pServerName 	:= ADR(gSQLCtrl.par.sHost);
			ReceipeDBConnect.pDatabaseName	:= ADR(gSQLCtrl.par.sDataBase);
			eStep := eCONNECT_r;
			
		eCONNECT_r :
			ReceipeDBConnect();
			IF ReceipeDBConnect.status = ERR_OK THEN
				gSQLCtrl.status.ConnectionIdent := ReceipeDBConnect.connectionIdent;
				eStep := eGET_TABLE_NUMBER; 			
			ELSIF ReceipeDBConnect.status <> ERR_FUB_BUSY THEN
				IF ReceipeDBConnect.connectionIdent <> 0 THEN
					gSQLCtrl.status.ConnectionIdent := ReceipeDBConnect.connectionIdent;
					gSQLCtrl.status.statusID	:= ReceipeDBConnect.status;
					gSQLCtrl.status.dbError		:= ReceipeDBConnect.dbError;
					eStep := eGET_ERROR;
				ELSE
					gSQLCtrl.status.statusID	:= ReceipeDBConnect.status;
					gSQLCtrl.status.dbError		:= ReceipeDBConnect.dbError;
					eStep := eERROR;
				END_IF
			END_IF	
	
			
			
		(* read out number of Tables *)		
		eGET_TABLE_NUMBER:
			brsstrcpy( ADR(sSQLstring), ADR('select count(*) as [tables] from sys.tables') );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eGET_TABLE_NUMBER_r;
		
		eGET_TABLE_NUMBER_r:
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eFETCH_TABLE_NUMBER;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
		
		eFETCH_TABLE_NUMBER:
			ReceipeDBFetchNextRow.enable			:= TRUE;
			ReceipeDBFetchNextRow.connectionIdent	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eFETCH_TABLE_NUMBER_r;
			
		eFETCH_TABLE_NUMBER_r:
			ReceipeDBFetchNextRow();
			IF ReceipeDBFetchNextRow.status = ERR_OK THEN
				eStep := eREAD_TABLE_NUMBER;
			ELSIF ReceipeDBFetchNextRow.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBFetchNextRow.status;
				gSQLCtrl.status.dbError		:= ReceipeDBFetchNextRow.dbError;
				eStep := eGET_ERROR;
			END_IF
			
		eREAD_TABLE_NUMBER:
			ReceipeDBGetData.enable 		:= TRUE;
			ReceipeDBGetData.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeDBGetData.columnIdx 		:= 1;
			ReceipeDBGetData.pData 			:= ADR(gSQLCtrl.status.TableCounter);
			ReceipeDBGetData.dataSize 		:= SIZEOF(gSQLCtrl.status.TableCounter);
			ReceipeDBGetData.dataType 		:= DB_SQL_INTEGER;
			eStep := eREAD_TABLE_NUMBER_r;
			
		eREAD_TABLE_NUMBER_r:
			ReceipeDBGetData();
			IF ReceipeDBGetData.status = 0 THEN
				IF ReceipeDBGetData.dbError = DB_SQL_NO_DATA THEN
					eStep := eWAIT_FOR_COMMAND;
				ELSE
					IF gSQLCtrl.status.TableCounter = 0 THEN
						eStep := eCREATE_TABLE; 
					ELSE
						eStep := eGET_TABLES; 
					END_IF
				END_IF;
			ELSIF ReceipeDBGetData.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetData.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetData.dbError;
				eStep := eGET_ERROR;
			END_IF
			
			
			
		(* read out available tables *)
		eGET_TABLES :
			brsstrcpy( ADR(sSQLstring), ADR('SELECT name FROM sysobjects WHERE type=$'u$'') );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eGET_TABLES_r;
		
		eGET_TABLES_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				IF gSQLCtrl.status.TableCounter = 0 THEN
					eStep := eWAIT_FOR_COMMAND;
				ELSE
					TableCountTemp := 0;
					eStep := eFETCH_NEXT_ROW;
				END_IF;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
			
			(* prepare one row to read column(s) *)
		eFETCH_NEXT_ROW :
			ReceipeDBFetchNextRow.enable 			:= TRUE;
			ReceipeDBFetchNextRow.connectionIdent	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eFETCH_NEXT_ROW_r;
			
		eFETCH_NEXT_ROW_r :
			ReceipeDBFetchNextRow();
			IF ReceipeDBFetchNextRow.status = ERR_OK THEN
				eStep := eREAD_TABLE_NAME;
			ELSIF ReceipeDBFetchNextRow.status <> ERR_FUB_BUSY THEN
				IF ReceipeDBFetchNextRow.dbError = DB_SQL_NO_DATA THEN
					eStep := eCREATE_TABLE;
				ELSE				
					gSQLCtrl.status.statusID	:= ReceipeDBFetchNextRow.status;
					gSQLCtrl.status.dbError		:= ReceipeDBFetchNextRow.dbError;
					eStep := eGET_ERROR;
				END_IF
			END_IF
		
			(* read out 1st column -> name *)
		eREAD_TABLE_NAME :
			ReceipeDBGetData.enable 		:= TRUE;
			ReceipeDBGetData.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeDBGetData.columnIdx 		:= 1;
			ReceipeDBGetData.pData 			:= ADR(sData);
			ReceipeDBGetData.dataSize 		:= SIZEOF(sData);
			ReceipeDBGetData.dataType 		:= DB_SQL_CHAR;
			eStep := eREAD_TABLE_NAME_r;
		
		eREAD_TABLE_NAME_r :
			ReceipeDBGetData();
			IF ReceipeDBGetData.status = ERR_OK THEN
				TableCountTemp := TableCountTemp + 1;
				IF ReceipeDBGetData.dbError = DB_SQL_NO_DATA THEN
					eStep := eWAIT_FOR_COMMAND;
				ELSE
					IF TableCountTemp <= gSQLCtrl.status.TableCounter THEN
						IF brsstrcmp( ADR(sData), ADR(gSQLCtrl.par.sTable) )=0 THEN
							eStep := eGET_RECEIPE_NUMBER; 
						ELSE
							IF TableCountTemp = gSQLCtrl.status.TableCounter THEN
								eStep := eCREATE_TABLE;
							ELSE
								eStep := eFETCH_NEXT_ROW;
							END_IF;
						END_IF
					END_IF;
				END_IF;
				

			ELSIF ReceipeDBGetData.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetData.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetData.dbError;
				eStep := eGET_ERROR;
			END_IF
		
			
			
		(* Create New gSQLCtrl.par.sTable *)
		eCREATE_TABLE :
			brsstrcpy( ADR(sSQLstring), ADR('CREATE TABLE '));
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable));
			brsstrcat( ADR(sSQLstring), ADR(' (') );
			brsstrcat( ADR(sSQLstring), ADR('idx smallint IDENTITY (1,1) PRIMARY KEY, ') );
			brsstrcat( ADR(sSQLstring), ADR('name varchar(') );
			brsitoa(LENGTH_NAME, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR(') UNIQUE, ') );
			brsstrcat( ADR(sSQLstring), ADR('price real, ') );
			brsstrcat( ADR(sSQLstring), ADR('setTemp real, ') );
			brsstrcat( ADR(sSQLstring), ADR('milk real, ') );
			brsstrcat( ADR(sSQLstring), ADR('sugar real, ') );
			brsstrcat( ADR(sSQLstring), ADR('coffee real, ') );
			brsstrcat( ADR(sSQLstring), ADR('water real ') );
			brsstrcat( ADR(sSQLstring), ADR(')') );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eCREATE_TABLE_r;
		
		eCREATE_TABLE_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eINSERT_DEFAULT;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF

			
			
		(* fill default receipe *)
		eINSERT_DEFAULT :
			brsstrcpy( ADR(sSQLstring), ADR('INSERT INTO ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' (name, price, setTemp, milk, sugar, coffee, water) ') );
			brsstrcat( ADR(sSQLstring), ADR(' VALUES ($'default$',$'1.73$',$'80.0$',$'95.0$',$'33.3$',$'66.6$',$'155.3$')') );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eINSERT_DEFAULT_r;
		
		eINSERT_DEFAULT_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eGET_RECEIPE_NUMBER;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
		
			
			
		(* get number of row on gSQLCtrl.par.sTable, ex: recipe, by EI*)	
		eGET_RECEIPE_NUMBER:
			brsmemset( ADR(sSQLstring), 0, SIZEOF(sSQLstring) );
			brsstrcpy( ADR(sSQLstring), ADR('SELECT COUNT(*) as cnt FROM ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeNameCounter				:= 0;
			gSQLCtrl.status.RecipeCounter	:= 0;
			eStep := eGET_RECEIPE_NUMBER_r;
			
		eGET_RECEIPE_NUMBER_r:	
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eFETCH_RECIPE_NUMBER;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
		
		eFETCH_RECIPE_NUMBER:
			ReceipeDBFetchNextRow.enable := TRUE;
			ReceipeDBFetchNextRow.connectionIdent := gSQLCtrl.status.ConnectionIdent;
			eStep := eFETCH_RECIPE_NUMBER_r;
			
		eFETCH_RECIPE_NUMBER_r:
			ReceipeDBFetchNextRow();
			IF ReceipeDBFetchNextRow.status = ERR_OK THEN
				eStep := eREAD_RECEIPE_NUMBER;
			ELSIF ReceipeDBFetchNextRow.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBFetchNextRow.status;
				gSQLCtrl.status.dbError		:= ReceipeDBFetchNextRow.dbError;
				eStep := eGET_ERROR;
			END_IF
			
		eREAD_RECEIPE_NUMBER:
			ReceipeDBGetData.enable 		:= TRUE;
			ReceipeDBGetData.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeDBGetData.columnIdx		:= 1;
			ReceipeDBGetData.pData 			:= ADR(gSQLCtrl.status.RecipeCounter);
			ReceipeDBGetData.dataSize 		:= SIZEOF(gSQLCtrl.status.RecipeCounter);
			ReceipeDBGetData.dataType 		:= DB_SQL_INTEGER; 
			eStep := eREAD_RECEIPE_NUMBER_r;
			
		eREAD_RECEIPE_NUMBER_r:
			ReceipeDBGetData();
			IF ReceipeDBGetData.status = 0 THEN
				IF gSQLCtrl.status.RecipeCounter = 0 THEN
					brsmemset( ADR(gSQLCtrl.par.sReceipeNames), 0, SIZEOF(gSQLCtrl.par.sReceipeNames) );
					eStep := eINSERT_DEFAULT;
				ELSE
					eStep := eGET_RECEIPE_NAMES; 
				END_IF;
	  				
			ELSIF ReceipeDBGetData.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetData.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetData.dbError;
				eStep := eGET_ERROR;
			END_IF;
			
			
			
		(* get receipe names - only as much as can saved in list *)
		eGET_RECEIPE_NAMES :
			brsstrcpy( ADR(sSQLstring), ADR('SELECT TOP(') );
			brsitoa( SIZEOF(gSQLCtrl.par.sReceipeNames)/SIZEOF(gSQLCtrl.par.sReceipeNames[0]), ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR(') name FROM ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' ORDER BY name ASC') );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeNameCounter := 0;
			
			IF gSQLCtrl.status.RecipeCounter >= 10 THEN
				gSQLCtrl.status.RecipeCounter 		:= 10;
			END_IF;
			
			brsmemset( ADR(gSQLCtrl.par.sReceipeNames), 0, SIZEOF(gSQLCtrl.par.sReceipeNames) );
			eStep := eGET_RECEIPE_NAMES_r;

			
		eGET_RECEIPE_NAMES_r:
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eFETCH_RECEIPE_NAME;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
			
			(* prepare one row to read column(s) *)
		eFETCH_RECEIPE_NAME :
			ReceipeDBFetchNextRow.enable		 := TRUE;
			ReceipeDBFetchNextRow.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eFETCH_RECEIPE_NAME_r;
		
		eFETCH_RECEIPE_NAME_r :
			ReceipeDBFetchNextRow();
			IF ReceipeDBFetchNextRow.status = ERR_OK THEN
				eStep := eREAD_RECEIPE_NAME;
			ELSIF ReceipeDBFetchNextRow.status <> ERR_FUB_BUSY THEN
				IF ReceipeDBFetchNextRow.dbError = DB_SQL_NO_DATA THEN
					IF ReceipeNameCounter = 0 THEN	(* no receipe - maybe last receipe deleted *)
						eStep := eINSERT_DEFAULT;
					ELSE
						eStep := eWAIT_FOR_COMMAND;	(* all receipes read - wait for command *)
					END_IF
				ELSE
					gSQLCtrl.status.statusID	:= ReceipeDBFetchNextRow.status;
					gSQLCtrl.status.dbError		:= ReceipeDBFetchNextRow.dbError;
					eStep := eGET_ERROR;
				END_IF
			END_IF
			
			(* read out 1st column -> name *)
		eREAD_RECEIPE_NAME :
			ReceipeDBGetData.enable 		:= TRUE;
			ReceipeDBGetData.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeDBGetData.columnIdx 		:= 1;
			ReceipeDBGetData.pData 			:= ADR( gSQLCtrl.par.sReceipeNames[ReceipeNameCounter] );
			ReceipeDBGetData.dataSize 		:= SIZEOF(gSQLCtrl.par.sReceipeNames[0]) - 1;
			ReceipeDBGetData.dataType 		:= DB_SQL_CHAR;
			eStep := eREAD_RECEIPE_NAME_r;
		
		eREAD_RECEIPE_NAME_r :
			ReceipeDBGetData();
			IF ReceipeDBGetData.status = ERR_OK THEN
				ReceipeNameCounter := ReceipeNameCounter + 1;
				IF ReceipeNameCounter < gSQLCtrl.status.RecipeCounter THEN 
					eStep := eFETCH_RECEIPE_NAME;
				ELSE
					gSQLCtrl.par.uStatus := 0;
					eStep := eWAIT_FOR_COMMAND;
				END_IF				
			ELSIF ReceipeDBGetData.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetData.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetData.dbError;
				eStep := eGET_ERROR;
			END_IF
		
			
			
			
		(********************************************** 
			waiting next command after connection 
		 **********************************************)	
		eWAIT_FOR_COMMAND :
			
			IF gSQLCtrl.cmd.DisConnect  THEN
				gSQLCtrl.cmd.DisConnect := FALSE;
				eStep:= eDISCONNECT;
			ELSIF gSQLCtrl.cmd.Connect  THEN
				gSQLCtrl.cmd.Connect	:= FALSE;
				eStep:= eCONNECT;
			ELSIF gSQLCtrl.cmd.LoadRecipe  THEN
				gSQLCtrl.cmd.LoadRecipe := FALSE;
				eStep:= eLOAD_RECEIPE;
			ELSIF gSQLCtrl.cmd.SaveRecipe  THEN
				gSQLCtrl.cmd.SaveRecipe := FALSE;
				eStep:= eSAVE_RECEIPE;
			ELSIF gSQLCtrl.cmd.SaveAsRecipe  THEN
				gSQLCtrl.cmd.SaveAsRecipe := FALSE;
				eStep:= eSAVE_AS;
			ELSIF gSQLCtrl.cmd.DeleteRecipe  THEN
				gSQLCtrl.cmd.DeleteRecipe := FALSE;
				eStep:= eDELETE_RECEIPE;
			ELSIF gSQLCtrl.cmd.RenameRecipe  THEN
				gSQLCtrl.cmd.RenameRecipe := FALSE;
				eStep:= eRENAME_RECEIPE;
			END_IF;
			
			
		
		(* Load a receipe *)
		eLOAD_RECEIPE :
			brsstrcpy( ADR(sSQLstring), ADR('SELECT price, setTemp, milk, sugar, coffee, water FROM ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' WHERE name = $'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sActReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$'') );
			ReceipeDBExecute.enable 		:= TRUE;
			ReceipeDBExecute.pSqlStatement	:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eLOAD_RECEIPE_r;
		
		eLOAD_RECEIPE_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eFETCH_VALUES;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				IF ReceipeDBExecute.dbError = DB_SQL_NO_DATA THEN
					eStep := eWAIT_FOR_COMMAND;
				ELSE
					gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
					gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
					eStep := eGET_ERROR;
				END_IF
			END_IF

			(* prepare one row to read column(s) *)
		eFETCH_VALUES :
			ReceipeDBFetchNextRow.enable			:= TRUE;
			ReceipeDBFetchNextRow.connectionIdent 	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eFETCH_VALUES_r;

		eFETCH_VALUES_r :
			ReceipeDBFetchNextRow();
			IF ReceipeDBFetchNextRow.status = ERR_OK THEN
				usCol := 1;
				eStep := eGET_VALUES;
			ELSIF ReceipeDBFetchNextRow.status <> ERR_FUB_BUSY THEN
				IF ReceipeDBFetchNextRow.dbError = DB_SQL_NO_DATA THEN
					gSQLCtrl.par.uStatus := 1;
					eStep := eWAIT_FOR_COMMAND;
				ELSE
					gSQLCtrl.status.statusID	:= ReceipeDBFetchNextRow.status;
					gSQLCtrl.status.dbError		:= ReceipeDBFetchNextRow.dbError;
					eStep := eGET_ERROR;
				END_IF
			END_IF

			(* read out all columns *)
		eGET_VALUES :
			ReceipeDBGetData.enable 		:= TRUE;
			ReceipeDBGetData.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			ReceipeDBGetData.columnIdx 		:= usCol;
			CASE usCol OF
				1:			
					ReceipeDBGetData.pData 		:= ADR(gSQLCtrl.par.recipe.price);
					ReceipeDBGetData.dataSize 	:= SIZEOF(gSQLCtrl.par.recipe.price);
					ReceipeDBGetData.dataType 	:= DB_SQL_REAL;
				2:
					ReceipeDBGetData.pData 		:= ADR(gSQLCtrl.par.recipe.setTemp);
					ReceipeDBGetData.dataSize 	:= SIZEOF(gSQLCtrl.par.recipe.setTemp);
					ReceipeDBGetData.dataType 	:= DB_SQL_REAL;
				3:
					ReceipeDBGetData.pData 		:= ADR(gSQLCtrl.par.recipe.milk);
					ReceipeDBGetData.dataSize	:= SIZEOF(gSQLCtrl.par.recipe.milk);
					ReceipeDBGetData.dataType	:= DB_SQL_REAL;
				4:
					ReceipeDBGetData.pData 		:= ADR(gSQLCtrl.par.recipe.sugar);
					ReceipeDBGetData.dataSize 	:= SIZEOF(gSQLCtrl.par.recipe.sugar);
					ReceipeDBGetData.dataType 	:= DB_SQL_REAL;
				5:
					ReceipeDBGetData.pData 		:= ADR(gSQLCtrl.par.recipe.coffee);
					ReceipeDBGetData.dataSize 	:= SIZEOF(gSQLCtrl.par.recipe.coffee);
					ReceipeDBGetData.dataType 	:= DB_SQL_REAL;
				6:
					ReceipeDBGetData.pData 		:= ADR(gSQLCtrl.par.recipe.water);
					ReceipeDBGetData.dataSize 	:= SIZEOF(gSQLCtrl.par.recipe.water);
					ReceipeDBGetData.dataType 	:= DB_SQL_REAL;
			END_CASE
			eStep := eGET_VALUES_r;
			
		eGET_VALUES_r :
			ReceipeDBGetData();
			IF ReceipeDBGetData.status = ERR_OK THEN
				usCol := usCol + 1;
				IF usCol <= 6 THEN
					eStep := eGET_VALUES;
				ELSE
					gSQLCtrl.par.uStatus := ERR_OK;
					brsstrcpy( ADR(gSQLCtrl.par.sNewReceipe), ADR(gSQLCtrl.par.sActReceipe) );
					eStep := eWAIT_FOR_COMMAND;
				END_IF				
			ELSIF ReceipeDBGetData.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetData.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetData.dbError;
				eStep := eGET_ERROR;
			END_IF
			
			
			
		(* save receipe *)
		eSAVE_RECEIPE :
			brsstrcpy( ADR(sSQLstring), ADR('UPDATE ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' SET price=$'') );
			brsftoa( gSQLCtrl.par.recipe.price, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR('$', setTemp=$'') );
			brsftoa( gSQLCtrl.par.recipe.setTemp, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR('$', milk=$'') );
			brsftoa( gSQLCtrl.par.recipe.milk, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR('$', sugar=$'') );
			brsftoa( gSQLCtrl.par.recipe.sugar, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR('$', coffee=$'') );
			brsftoa( gSQLCtrl.par.recipe.coffee, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR('$', water=$'') );
			brsftoa( gSQLCtrl.par.recipe.water, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );
			brsstrcat( ADR(sSQLstring), ADR('$' WHERE name=$'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sActReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$'') );
			ReceipeDBExecute.enable := TRUE;
			ReceipeDBExecute.pSqlStatement := ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent := gSQLCtrl.status.ConnectionIdent;
			eStep := eSAVE_RECEIPE_r;
	
		eSAVE_RECEIPE_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eSAVE_GET_AFFECTED_ROWS;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
	
		eSAVE_GET_AFFECTED_ROWS :
			ReceipeDBGetAffectedRows.enable 		:= TRUE;
			ReceipeDBGetAffectedRows.connectionIdent:= gSQLCtrl.status.ConnectionIdent;
			eStep := eSAVE_GET_AFFECTED_ROWS_r;
	
		eSAVE_GET_AFFECTED_ROWS_r :
			ReceipeDBGetAffectedRows();
			IF ReceipeDBGetAffectedRows.status = ERR_OK THEN
				IF ReceipeDBGetAffectedRows.affectedRows = 1 THEN	(* one row updated -> correct *)
					gSQLCtrl.par.uStatus := 0;
				ELSE
					gSQLCtrl.par.uStatus := 1;
				END_IF
				eStep := eWAIT_FOR_COMMAND;
			ELSIF ReceipeDBGetAffectedRows.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetAffectedRows.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetAffectedRows.dbError;
				eStep := eGET_ERROR;
			END_IF

			
			
		(* save with new name - only if new name not exist*)
		eSAVE_AS :
			brsstrcpy( ADR(sSQLstring), ADR('IF NOT EXISTS (SELECT * FROM ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' WHERE name=$'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sNewReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$') ') );
			brsstrcat( ADR(sSQLstring), ADR('INSERT INTO ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' (name, price, setTemp, milk, sugar, coffee, water) ') );
			brsstrcat( ADR(sSQLstring), ADR('VALUES ($'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sNewReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$',$'') );
			brsftoa( gSQLCtrl.par.recipe.price, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );			
			brsstrcat( ADR(sSQLstring), ADR('$',$'') );
			brsftoa( gSQLCtrl.par.recipe.setTemp, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );			
			brsstrcat( ADR(sSQLstring), ADR('$',$'') );
			brsftoa( gSQLCtrl.par.recipe.milk, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );			
			brsstrcat( ADR(sSQLstring), ADR('$',$'') );
			brsftoa( gSQLCtrl.par.recipe.sugar, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );			
			brsstrcat( ADR(sSQLstring), ADR('$',$'') );
			brsftoa( gSQLCtrl.par.recipe.coffee, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );			
			brsstrcat( ADR(sSQLstring), ADR('$',$'') );
			brsftoa( gSQLCtrl.par.recipe.water, ADR(sSQLstring) + brsstrlen(ADR(sSQLstring)) );			
			brsstrcat( ADR(sSQLstring), ADR('$')') );
			ReceipeDBExecute.enable 			:= TRUE;
			ReceipeDBExecute.pSqlStatement		:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent 	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eSAVE_AS_r;
			
		eSAVE_AS_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eSAVE_AS_ROWS_AFFECTED;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
		
		eSAVE_AS_ROWS_AFFECTED :
			ReceipeDBGetAffectedRows.enable 			:= TRUE;
			ReceipeDBGetAffectedRows.connectionIdent	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eSAVE_AS_ROWS_AFFECTED_r;
		
		eSAVE_AS_ROWS_AFFECTED_r :
			ReceipeDBGetAffectedRows();
			IF ReceipeDBGetAffectedRows.status = ERR_OK THEN
				IF ReceipeDBGetAffectedRows.affectedRows = 1 THEN	(* one row updated -> correct *)
					gSQLCtrl.par.uStatus := 0;
					brsstrcpy( ADR(gSQLCtrl.par.sActReceipe), ADR(gSQLCtrl.par.sNewReceipe) );
				ELSE
					gSQLCtrl.par.uStatus := 1;
					brsstrcpy( ADR(gSQLCtrl.par.sActReceipe), ADR('') );
				END_IF
				eStep := eGET_RECEIPE_NUMBER;
			ELSIF ReceipeDBGetAffectedRows.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetAffectedRows.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetAffectedRows.dbError;
				eStep := eGET_ERROR;
			END_IF
		
			
			
		(* delete receipe *)
		eDELETE_RECEIPE :
			brsstrcpy( ADR(sSQLstring), ADR('DELETE FROM ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' WHERE name=$'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sActReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$'') );
			ReceipeDBExecute.enable 			:= TRUE;
			ReceipeDBExecute.pSqlStatement		:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eDELETE_RECEIPE_r;
		
		eDELETE_RECEIPE_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eDELETE_ROWS_AFFECTED;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF
			
		eDELETE_ROWS_AFFECTED :
			ReceipeDBGetAffectedRows.enable				:= TRUE;
			ReceipeDBGetAffectedRows.connectionIdent	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eDELETE_ROWS_AFFECTED_r;
			
		eDELETE_ROWS_AFFECTED_r :
			ReceipeDBGetAffectedRows();
			IF ReceipeDBGetAffectedRows.status = ERR_OK THEN
				IF ReceipeDBGetAffectedRows.affectedRows = 1 THEN	(* one row updated -> correct *)
					gSQLCtrl.par.uStatus := 0;
					IF brsstrcmp( ADR(gSQLCtrl.par.sActReceipe), ADR(gSQLCtrl.par.sNewReceipe) ) = 0 THEN	(* when delete actual receipe *)
						brsstrcpy( ADR(gSQLCtrl.par.sNewReceipe), ADR('') );
					END_IF					
					brsstrcpy( ADR(gSQLCtrl.par.sActReceipe), ADR('') );
				ELSE
					gSQLCtrl.par.uStatus := 1;
				END_IF
				eStep := eGET_RECEIPE_NUMBER; 
			ELSIF ReceipeDBGetAffectedRows.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetAffectedRows.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetAffectedRows.dbError;
				eStep := eGET_ERROR;
			END_IF

			
			
		(* rename receipe - only if new name not exist*)
		eRENAME_RECEIPE :
			brsstrcpy( ADR(sSQLstring), ADR('IF NOT EXISTS (SELECT * FROM ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' WHERE name=$'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sNewReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$') ') );
			brsstrcat( ADR(sSQLstring), ADR('UPDATE ') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sTable) );
			brsstrcat( ADR(sSQLstring), ADR(' SET name=$'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sNewReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$' WHERE name=$'') );
			brsstrcat( ADR(sSQLstring), ADR(gSQLCtrl.par.sActReceipe) );
			brsstrcat( ADR(sSQLstring), ADR('$'') );
			ReceipeDBExecute.enable 			:= TRUE;
			ReceipeDBExecute.pSqlStatement 		:= ADR(sSQLstring);
			ReceipeDBExecute.connectionIdent 	:= gSQLCtrl.status.ConnectionIdent;
			eStep := eRENAME_RECEIPE_r;
			
		eRENAME_RECEIPE_r :
			ReceipeDBExecute();
			IF ReceipeDBExecute.status = ERR_OK THEN
				eStep := eRENAME_GET_AFFECTED_ROWS;
			ELSIF ReceipeDBExecute.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBExecute.status;
				gSQLCtrl.status.dbError		:= ReceipeDBExecute.dbError;
				eStep := eGET_ERROR;
			END_IF

		eRENAME_GET_AFFECTED_ROWS :
			ReceipeDBGetAffectedRows.enable := TRUE;
			ReceipeDBGetAffectedRows.connectionIdent := gSQLCtrl.status.ConnectionIdent;
			eStep := eRENAME_GET_AFFECTED_ROWS_r;
			
		eRENAME_GET_AFFECTED_ROWS_r :
			ReceipeDBGetAffectedRows();
			IF ReceipeDBGetAffectedRows.status = ERR_OK THEN
				IF ReceipeDBGetAffectedRows.affectedRows = 1 THEN	(* one row updated -> correct *)
					gSQLCtrl.par.uStatus := 0;
					brsstrcpy( ADR(gSQLCtrl.par.sActReceipe),ADR(gSQLCtrl.par.sNewReceipe) );
				ELSE
					gSQLCtrl.par.uStatus := 1;
					brsstrcpy( ADR(gSQLCtrl.par.sActReceipe),ADR('') );
				END_IF
				eStep := eGET_RECEIPE_NUMBER;
			ELSIF ReceipeDBGetAffectedRows.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.status.statusID	:= ReceipeDBGetAffectedRows.status;
				gSQLCtrl.status.dbError		:= ReceipeDBGetAffectedRows.dbError;
				eStep := eGET_ERROR;
			END_IF

			
			
		(* disconnect from server *)
		eDISCONNECT :
			ReceipeDBDisconnect.enable 			:= TRUE;
			ReceipeDBDisconnect.connectionIdent := gSQLCtrl.status.ConnectionIdent;
			eStep := eDISCONNECT_r;
		
		eDISCONNECT_r :
			ReceipeDBDisconnect();
			IF ReceipeDBDisconnect.status <> ERR_FUB_BUSY THEN
				brsmemset( ADR(gSQLCtrl.par.sReceipeNames), 0, SIZEOF(gSQLCtrl.par.sReceipeNames) );
				eStep := eSTOP;
				gSQLCtrl.status.ConnectionIdent := 0;
			END_IF		

		
			
		(* get error string *)
		eGET_ERROR :
			brsmemset( ADR(gSQLCtrl.status.sErrorString), 0, SIZEOF(gSQLCtrl.status.sErrorString) );
			ReceipeDBErrMsg.enable			:= TRUE;
			ReceipeDBErrMsg.connectionIdent := gSQLCtrl.status.ConnectionIdent;
			ReceipeDBErrMsg.pErrorMessage	:= ADR( gSQLCtrl.status.sErrorString );
			ReceipeDBErrMsg.errorMessageSize:= SIZEOF( gSQLCtrl.status.sErrorString ) - 1;
			eStep := eGET_ERROR_r;
			
		eGET_ERROR_r :
			ReceipeDBErrMsg();
			IF ReceipeDBErrMsg.status <> ERR_FUB_BUSY THEN
				gSQLCtrl.par.uStatus := 2;
				eStep := eERROR;
			END_IF
			
		eERROR:
			IF gSQLCtrl.cmd.Acknowledge THEN
				brsmemset( ADR(gSQLCtrl.status.sErrorString), 0, SIZEOF(gSQLCtrl.status.sErrorString) );
				gSQLCtrl.status.statusID	:= 0;
				gSQLCtrl.status.dbError		:= 0;
				gSQLCtrl.cmd.Acknowledge	:= FALSE;
				eStep := eWAIT_FOR_COMMAND;
			END_IF;
		
	END_CASE;

	
	(* Updating current steps for display*)
	gSQLCtrl.status.DisplayStep := eStep;

	
END_PROGRAM



