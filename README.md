# AsDb library with MS SQL

B&R Automation Studio AsDb library with MS SQL

##한글 Korean

###버전 정보
 - v1.0 : 새 샘플 2022/4/21

###소프트웨어 버전
 - B&R Automation Studio 4.11
 - Automation Runtime B4.92

###필요한 라이브러리
 - AsDB, AsBrStr

###네트워크 설정
  - PC* 
     - *PC에 MSSQL DB, Automation Studio 4.11가 설치되어 있다.
  -     IP: 192.168.20.119
  -     Subnet mask: 255.255.255.0
   
    
  - PLC
  -     IP: 192.168.20.200
  -     Subnet mask: 255.255.255.0

###파일설명
 -[ ] DemoProject : "MyASDBSamples" B&R PLC와 MS SQL의 Database간에 AsDB 라이브러리를 사용하여 통신하는 프로젝트 예시
 -[ ] SampleCode: "ASDB_STKO1" 기존 프로젝트에 샘플 코드만 추가하여 테스트 가능한 버전
    메인 매뉴 "File / Import..."  사용, "ASDB_STKO1.zip"
	
	
	
	

 #English

###Version Information
 - v1.0 : new Sample 2022/4/21

###Software version
 - B&R Automation Studio 4.11
 - Automation Runtime B4.92

###Necessary Library
 - AsDB, AsBrStr

###Network topology

  - PC* 
     - *It installed MSSQL DB, Automation Studio 4.11 on PC.
  -     IP: 192.168.20.119
  -     Subnet mask: 255.255.255.0
   
    
  - PLC
  -     IP: 192.168.20.200
  -     Subnet mask: 255.255.255.0


###Description of file 
 -[ ] DemoProject : "MyASDBSamples" Demo Project for communication between B&R PLC and MS SQL Database using AsDb Library
 -[ ] SampleCode: "ASDB_STKO1"  Version that can be tested by adding only sample code to an existing project
    using main menu "File / Import...", "ASDB_STKO1.zip"